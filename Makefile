APP := tweet-o-rasa

# -s -w: omit debug and similar symbols.
LD_FLAGS := "-s -w"

PKG_LIST := $(shell find . -type f -name '*.go')

.PHONY: clean \
	test \
	lint

moduleupdate:
	@ go mod tidy

build: ${PKG_LIST} moduleupdate
	@ go build -o $(APP) -ldflags=$(LD_FLAGS) main.go

debug: ${PKG_LIST} moduleupdate
	@ go build -o $(APP) main.go

run: debug
	@ ./$(APP) -a

clean:
	@ rm -f $(APP)

test:
	@ go test -cover -race -count=1 ./...

lint:
	@ golint -set_exit_status $(PKG_LIST)
