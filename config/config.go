package config // import "tweet-o-rasa/config"

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	toml "github.com/pelletier/go-toml"
)

type Twitter struct {
	AccessToken       string `toml:"access-token"`
	AccessTokenSecret string `toml:"access-token-secret"`
	ConsumerApiKey    string `toml:"consumer-api-key"`
	ConsumerApiSecret string `toml:"consumer-api-secret"`
	Username          string `toml:"username"`
	DaysToKeepTweets  int    `toml:"days-to-keep-tweets"`
	DaysToKeepLikes   int    `toml:"days-to-keep-likes"`
}

type Config struct {
	Twitter Twitter
}

func CheckConfigFileExistsAndIsValid() (Config, error) {
	config := Config{}
	configfile, err := os.Open("config.toml")
	if err != nil {
		err = createInitialConfigFile()
		if err != nil {
			log.Print("We couldn't find a valid config.toml file and tried to create one, which failed.")
			log.Print("Please make sure that the directory you are in is writable.")
			return config, err
		}
		msg := `This seems to be a first run. We created a config file for you.
		Please open 'config.toml', check the defaults (i.e. age of tweets to delete)
		and insert your Twitter username as well as your Twitter API key.`
		return config, fmt.Errorf(msg)
	}
	defer configfile.Close()

	// Read the file.
	byteValue, _ := ioutil.ReadAll(configfile)
	toml.Unmarshal(byteValue, &config)

	err = checkConfigIsComplete(config)
	if err != nil {
		return config, err
	}

	return config, nil
}

func checkConfigIsComplete(config Config) error {
	if config.Twitter.AccessToken == "" {
		return fmt.Errorf("config file does not contain access-token for Twitter, which is required")
	}
	if config.Twitter.AccessTokenSecret == "" {
		return fmt.Errorf("config file does not contain acces-token-secret for Twitter, which is required")
	}
	if config.Twitter.ConsumerApiKey == "" {
		return fmt.Errorf("config file does not contain consumer-api-key for Twitter, which is required")
	}
	if config.Twitter.ConsumerApiSecret == "" {
		return fmt.Errorf("config file does not contain consumer-api-secret for Twitter, which is required")
	}
	if config.Twitter.Username == "" {
		return fmt.Errorf("config file does not contain your Twitter username, which is required")
	}
	if config.Twitter.DaysToKeepTweets == 0 {
		return fmt.Errorf("please make sure to specify a valid age (in days) for tweets to keep")
	}
	if config.Twitter.DaysToKeepLikes == 0 {
		return fmt.Errorf("please make sure to specify a valid age (in days) for likes to keep")
	}
	return nil
}

func createInitialConfigFile() error {
	f, err := os.Create("config.toml")
	if err != nil {
		return err
	}
	defer f.Close()

	// The config file's content.
	buf := bytes.Buffer{}
	buf.WriteString("[twitter]\n")
	buf.WriteString("access-token = \"\"\n")
	buf.WriteString("access-token-secret = \"\"\n")
	buf.WriteString("consumer-api-key = \"\"\n")
	buf.WriteString("consumer-api-secret = \"\"\n")
	buf.WriteString("username = \"\"\n")
	buf.WriteString("days-to-keep-tweets = 365\n")
	buf.WriteString("days-to-keep-likes = 365\n")
	str := buf.String()
	_, err = f.WriteString(str)
	if err != nil {
		return err
	}
	return nil
}
