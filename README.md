# Tweet-o-Rasa

Delete tweets and likes that are older than what you want to keep.

## Use (watch out for the warning below)

Get yourself a Twitter API token:

1. Go to [https://developer.twitter.com/apps](https://developer.twitter.com/apps)
2. *Create an app* and grant *Read and write* access permissions
3. Generate your API token in *Keys and tokens*
4. Copy'n'Paste the *Consumer token*, the *Access token*, and the respective *secrets* to your `config.toml`.

If you don't yet have a `config.toml` just run `tweet-o-rasa` once. This will create a sample config file.

Define the age for tweets and likes that you want to keep, i.e. anything that's older than these number of days will be deleted:

```toml
[twitter]
access-token = "copy and paste this"
access-token-secret = "copy and paste this"
consumer-api-key = "copy and paste this"
consumer-api-secret = "copy and paste this"
username = "your twitter username"
days-to-keep-tweets = 365
days-to-keep-likes = 365
```

Run:

```console
make build
tweet-o-rasa [-a] [-l] [-t]
```

This may seem a bit unresponsive from time to time, but should never stay that way for more than 15 minutes at any given time. These 15 minutes are an API rate limit thing from Twitter.

## Warning

This may delete all of your tweets. There is no test run. This is just a quick hack to possibly save some click efforts.
No guarantee is given that this does what is intended or even what is expected.

Use at your own risk.

## Related projects

* [go-twitter](https://github.com/dghubble/go-twitter/blob/master/twitter/statuses.go): a package that seems to provide some charming access to Twitter's API. It didn't work at the first run, thus you'll find some reimplementations of parts of its features here.
