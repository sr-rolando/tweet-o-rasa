package twitter // import "tweet-o-rasa/twitter"

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"net/http"
	"sort"
	"time"
	"tweet-o-rasa/config"
)

// Tweet represents all we are interested in.
type Tweet struct {
	ID        uint64 `json:"id"`
	User      *User  `json:"user"`
	CreatedAt string `json:"created_at"`
}

// User data comes as an own struct in the Twitter response.
type User struct {
	Name string `json:"screen_name"`
}

// TweetIdentifier is only a helper type to parse Twitter's data.
type TweetIdentifier struct {
	ID int64 `json:"id"`
}

// Timestamp provides a typed time representation of Twitter's "created_at" format.
func (t Tweet) Timestamp() time.Time {
	timestamp, _ := time.Parse(time.RubyDate, t.CreatedAt)
	return timestamp
}

// DeleteOldTweets uses the given config to delete all tweets that are older than the
// age defined therein.
func DeleteOldTweets(config config.Config, httpClient *http.Client) error {
	log.Printf("Going to delete tweets older than %d days...", config.Twitter.DaysToKeepTweets)
	var timeline []Tweet

	// The max tweet id which we use at each tweet fetching iteration.
	var maxID uint64
	maxID = math.MaxUint64
	// This can be 3200 max.
	numTweetsToRetrieve := 3200

	isFirstRun := true
	requestCount := 0
	// Fetch as much tweets as we can.
	log.Printf("  ..fetching tweets..")
	for true {
		chunk := getChunkOfTweets(config, httpClient, maxID, numTweetsToRetrieve, isFirstRun)
		// We don't use append(timeline, chunk...) as we want to retrieve max_id anyways.
		for i := 0; i < len(chunk); i++ {
			timeline = append(timeline, chunk[i])
			if chunk[i].ID < maxID {
				maxID = chunk[i].ID
			}
		}
		// maxID is the oldest one we got so far, start with one less into the next round
		maxID--

		// Sort the result by tweet timestamp.
		sort.Slice(timeline, func(i, j int) bool {
			return timeline[i].Timestamp().Before(timeline[j].Timestamp())
		})

		// There is a limit of 15 requests / 15-minute window.
		if len(chunk) == 0 {
			if requestCount >= 15 {
				// Thus, wait 15 minutes if we don't get anymore tweets and spent our request budget.
				log.Printf("  ..Twitter's API rate limit reached after %d requests, got %d tweets so far, pausing for 15 minutes..", requestCount, len(timeline))
				tmpO := timeline[0]
				tmpN := timeline[len(timeline)-1]
				log.Printf("  ..(oldest tweet received so far was posted at %s by %s with ID=%d)", tmpO.Timestamp(), tmpO.User.Name, tmpO.ID)
				log.Printf("  ..(newest tweet received so far was posted at %s by %s with ID=%d)", tmpN.Timestamp(), tmpN.User.Name, tmpN.ID)
				time.Sleep(15 * time.Minute)
				requestCount = 0
				log.Printf("  ..okay, let's continue fetching tweets..")
			} else {
				// No more tweets? No rate limit? Looks like we got all tweets.
				log.Printf("  ..done as we didn't get anymore tweet's from twitter, tried %d time(s) at the current 15-min window.", requestCount)
				break
			}
		}
		isFirstRun = false
		requestCount++
	}
	if len(timeline) == 0 {
		log.Printf("-> Got no tweets to delete.")
		return nil
	}
	oldest := timeline[0]
	log.Printf(" ..got %d tweet(s). Oldest one was posted at %s by %s with ID=%d)", len(timeline), oldest.Timestamp(), oldest.User.Name, oldest.ID)

	nrDeleted := doDeleteTweets(config, httpClient, timeline)
	log.Printf(" ..deleted %d tweet(s).", nrDeleted)

	return nil
}

// DeleteOldLikes uses the given config to delete all likes that are older than the
// age defined therein.
func DeleteOldLikes(config config.Config, httpClient *http.Client) error {
	log.Printf("Going to delete likes older than %d days...", config.Twitter.DaysToKeepLikes)
	var likes []Tweet

	// The max tweet id which we use at each likes-fetching iteration.
	var maxID uint64
	maxID = math.MaxUint64

	// Number of likes to retrieve at each chunk. Can be 200 max.
	numLikesToRetrieve := 200

	isFirstRun := true
	requestCount := 0
	// Fetch as much likes as we can.
	log.Printf("  ..fetching likes..")
	for true {
		chunk := getChunkOfLikes(config, httpClient, maxID, numLikesToRetrieve, isFirstRun)
		log.Printf("  ..got chunk of %d likes..", len(chunk))
		isFirstRun = false
		for i := 0; i < len(chunk); i++ {
			likes = append(likes, chunk[i])
			if chunk[i].ID < maxID {
				maxID = chunk[i].ID
			}
		}
		// maxID is the oldest one we got so far, start with one less into the next round
		maxID--

		// There is a limit of 75 requests / 15-minute window.
		if len(chunk) == 0 {
			if requestCount >= 75 {
				// Thus, wait 15 minutes if we don't get anymore tweets and spent our request budget.
				log.Printf("  ..Twitter's API rate limit reached after %d requests, got %d likes so far, pausing for 15 minutes..", requestCount, len(likes))
				time.Sleep(15 * time.Minute)
				requestCount = 0
				log.Printf("  ..okay, let's continue fetching likes..")
			} else {
				// No more likes? No rate limit? Looks like we got all likes.
				log.Printf("  ..done as we didn't get anymore likes's from twitter, tried %d time(s) at the current 15-min window.", requestCount)
				break
			}
		}
		requestCount++
	}

	if len(likes) == 0 {
		log.Printf("-> Got no likes to delete.")
		return nil
	}

	// Sort the result by tweet timestamp.
	sort.Slice(likes, func(i, j int) bool {
		return likes[i].Timestamp().Before(likes[j].Timestamp())
	})

	oldest := likes[0]
	log.Printf(" ..got %d likes(s). Oldest one for tweet posted at %s by %s with ID=%d)", len(likes), oldest.Timestamp(), oldest.User.Name, oldest.ID)

	nrDeleted := doDeleteLikes(config, httpClient, likes)
	log.Printf(" ..deleted %d likes(s).", nrDeleted)

	return nil
}

// see https://developer.twitter.com/en/docs/twitter-api/v1/tweets/timelines/api-reference/get-statuses-user_timeline
func getChunkOfTweets(config config.Config, httpClient *http.Client, maxID uint64, nrOfTweets int, firstRun bool) []Tweet {
	var url string
	if firstRun {
		url = fmt.Sprintf("https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=%s&count=%d",
			config.Twitter.Username, nrOfTweets)
	} else {
		url = fmt.Sprintf("https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=%s&max_id=%d&count=%d",
			config.Twitter.Username, maxID, nrOfTweets)
	}
	resp, err := httpClient.Get(url)
	if err != nil {
		// No tweets? Nothing to return.
		log.Printf(" ..error retrieving chunk of tweets: %v", err)
		emptySlice := []Tweet{}
		return emptySlice
	}
	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)

	// Convert the json response into some slice of tweets.
	tweets := []Tweet{}
	err = json.Unmarshal(body, &tweets)

	return tweets
}

// Go through list of tweets, check the timestamp, delete if older than specified in the config.
// Returns number of deleted tweets.
// see https://developer.twitter.com/en/docs/twitter-api/v1/tweets/post-and-engage/api-reference/post-statuses-destroy-id
func doDeleteTweets(config config.Config, httpClient *http.Client, timeline []Tweet) int {
	var nrDeleted int
	oldestTimeToKeep := time.Now().AddDate(0, 0, -1*config.Twitter.DaysToKeepTweets)
	log.Printf("  ..we'll delete tweets older than %s.", oldestTimeToKeep.String())
	for i := 0; i < len(timeline); i++ {
		tweet := timeline[i]
		if tweet.Timestamp().Before(oldestTimeToKeep) {
			url := fmt.Sprintf("https://api.twitter.com/1.1/statuses/destroy/%d.json", tweet.ID)
			value := map[string]string{"id": string(tweet.ID)}
			jsonvalue, _ := json.Marshal(value)
			_, err := httpClient.Post(url, "application/json", bytes.NewBuffer(jsonvalue))
			if err != nil {
				log.Printf("Error deleting tweet w/ id=%d: %v", tweet.ID, err)
				continue
			}
			log.Printf("   ..deleted tweet w/ ID=%d, posted by %s at %s", tweet.ID, tweet.User.Name, tweet.Timestamp())
			nrDeleted++
		}
	}
	return nrDeleted
}

func getChunkOfLikes(config config.Config, httpClient *http.Client, maxID uint64, nrOfLikes int, firstRun bool) []Tweet {
	var url string
	if firstRun {
		url = fmt.Sprintf("https://api.twitter.com/1.1/favorites/list.json?count=%d&screen_name=%s", nrOfLikes, config.Twitter.Username)
	} else {
		url = fmt.Sprintf("https://api.twitter.com/1.1/favorites/list.json?count=%d&screen_name=%s&max_id=%d", nrOfLikes, config.Twitter.Username, maxID)
	}
	resp, err := httpClient.Get(url)
	if err != nil {
		// No likes? Nothing to return.
		log.Printf(" ..error retrieving chunk of likes: %v", err)
		emptySlice := []Tweet{}
		return emptySlice
	}
	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)

	// Convert the json response into some slice of tweets.
	likes := []Tweet{}
	err = json.Unmarshal(body, &likes)

	return likes
}

func doDeleteLikes(config config.Config, httpClient *http.Client, likes []Tweet) int {
	var nrDeleted int
	oldestTimeToKeep := time.Now().AddDate(0, 0, -1*config.Twitter.DaysToKeepLikes)
	log.Printf("  ..we'll delete likes older than %s.", oldestTimeToKeep.String())
	for i := 0; i < len(likes); i++ {
		likedTweet := likes[i]
		if likedTweet.Timestamp().Before(oldestTimeToKeep) {
			url := fmt.Sprintf("https://api.twitter.com/1.1/favorites/destroy.json?id=%d", likedTweet.ID)
			value := map[string]string{"id": string(likedTweet.ID)}
			jsonvalue, _ := json.Marshal(value)
			_, err := httpClient.Post(url, "application/json", bytes.NewBuffer(jsonvalue))
			if err != nil {
				log.Printf("Error deleting like of tweet w/ id=%d: %v", likedTweet.ID, err)
				continue
			}
			log.Printf("   ..deleted like for tweet w/ ID=%d, posted by %s at %s", likedTweet.ID, likedTweet.User.Name, likedTweet.Timestamp())
			nrDeleted++
		}
	}
	return nrDeleted
}
