package main

import (
	"fmt"
	"log"
	"os"
	"tweet-o-rasa/config"
	"tweet-o-rasa/twitter"

	"github.com/dghubble/oauth1"
)

func main() {
	// Pass command line args.
	progname := os.Args[0]
	args := os.Args[1:]
	if len(args) == 0 {
		printUsage(progname)
		return
	}
	deleteLikes := false
	deleteTweets := false
	for _, arg := range args {
		switch arg {
		case "-a":
			deleteLikes = true
			deleteTweets = true
		case "-l":
			deleteLikes = true
		case "-t":
			deleteTweets = true
		default:
			printUsage(progname)
		}
	}

	// Check if valid config file exists.
	config, err := config.CheckConfigFileExistsAndIsValid()
	if err != nil {
		log.Print(err)
		return
	}

	// Authenticate using OAuth1 as we require user context auth to delete tweets and likes.
	authConfig := oauth1.NewConfig(config.Twitter.ConsumerApiKey, config.Twitter.ConsumerApiSecret)
	authToken := oauth1.NewToken(config.Twitter.AccessToken, config.Twitter.AccessTokenSecret)

	// Auth header will be automatically added by the oauth1 package.
	httpClient := authConfig.Client(oauth1.NoContext, authToken)

	if deleteTweets {
		err := twitter.DeleteOldTweets(config, httpClient)
		if err != nil {
			log.Fatal(err)
		}
	}
	if deleteLikes {
		err := twitter.DeleteOldLikes(config, httpClient)
		if err != nil {
			log.Fatal(err)
		}
	}
}

func printUsage(progname string) {
	fmt.Printf("Usage: %s [-a] [-l] [-t]\n", progname)
	fmt.Println("With:")
	fmt.Println("  -a: delete tweets and likes")
	fmt.Println("  -l: delete likes")
	fmt.Println("  -t: delete tweets")
}
