module tweet-o-rasa

go 1.16

require (
	github.com/dghubble/oauth1 v0.7.0
	github.com/pelletier/go-toml v1.8.1
)
